els_test_1
=====

An OTP application

This project was created via the command:
    $rebar3 new release els_test_1

o Next the directory apps/els_test_1/include was created

o Then the file defines.hrl was created and the macro 'macro_from_include' was defined.

o Next the defines.hrl was "included" in the els_test_1_sup.erl file, via -include(), 
  and the macro expansion was start with "?", but the macro does not appear in the list.

o To verify the macro expansion the logger.hrl from the erlang installation was included
  in the same file, however with -include_lib(), and those macros appear in the expansion
  list.